const isnega = require('./isnegative');

test('verificando si es negativo', () => {
  expect(isnega(-3)).toBe('es negativo');
});   